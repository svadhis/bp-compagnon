import 'package:bp_compagnon/entities/effect.dart';

class HealEffect extends Effect {
  HealEffect({int value = 1}) : super('Heal', value);

  @override
  void remove() {
    value = 0;
  }

  @override
  void add(int v) {
    value += v;
  }
}

class ChargeEffect extends Effect {
  ChargeEffect({int value = 1}) : super('Charge', value);

  @override
  void remove() {
    value = 0;
  }

  @override
  void add(int v) {
    value += v;
  }
}

class ImmuneEffect extends Effect {
  ImmuneEffect({int value = 1}) : super('Immune', value);

  @override
  void add(int v) {}

  @override
  void remove() {
    value = 0;
  }
}

class ShieldEffect extends Effect {
  ShieldEffect({int value = 1}) : super('Shield', value);

  @override
  void add(int v) {
    value += v;
  }

  @override
  void remove() {
    value--;
  }
}