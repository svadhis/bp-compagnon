import 'package:bp_compagnon/entities/player_state.dart';

class KnockdownState extends PlayerState {
  KnockdownState() : super('KD');
}

class StunState extends PlayerState {
  StunState() : super('Stun');
}

class RegularState extends PlayerState {
  RegularState() : super(' ');
}