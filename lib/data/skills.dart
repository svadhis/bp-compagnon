import 'package:bp_compagnon/data/steps.dart';
import 'package:bp_compagnon/entities/action.dart';
import 'package:bp_compagnon/entities/cast.dart';
import 'package:bp_compagnon/entities/character.dart';
import 'package:bp_compagnon/entities/skill.dart';
import 'package:bp_compagnon/entities/step.dart';

class SliceSW extends Skill {
  SliceSW() : super('Slice', Cast.swordsman, actions: [
    Action(steps: [DamageBoss(2)]),
    Action(steps: [ChargeSelf(3)]),
    Inaction()
  ]);
}