import 'package:bp_compagnon/data/effects.dart';
import 'package:bp_compagnon/entities/character.dart';
import 'package:bp_compagnon/entities/step.dart';

class DamageBoss extends Step {
  DamageBoss(int value) : super(
    value: value,
    application: () => Target.boss.takeDamage(value)
  );
}

class ChargeSelf extends Step {
  ChargeSelf(int value, {int rotations = 0}) : super(
    value: value,
    rotations: rotations,
    application: () => rotations == 0 ? Target.self.getEnergy(value) : Target.self.getEffect(ChargeEffect(value: value), rotations)
  );
}