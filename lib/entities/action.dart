import 'package:bp_compagnon/entities/character.dart';
import 'package:bp_compagnon/entities/step.dart';

class Inaction extends Action {
  Inaction() : super(steps: []);
}

class Attack extends Action {
  int range = 1;

  Attack({required List<Step> steps}) : super(steps: steps);

  bool isSuccessful() => !targets.every((target) => target.isProtected) && targets.every((target) => range - player.position - target.position <= 1);
}

class Action {
  late Player player;
  late List<Player> targets;
  List<Step> steps;

  Action({required this.steps}) {
    setProtagonists();
  }

  void resolve() {}

  void setProtagonists() {

  }
}

enum ActionType {
  attack,
  guard,
  counter,
  move,
  other
}