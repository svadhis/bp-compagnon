import 'package:bp_compagnon/data/player_states.dart';
import 'package:bp_compagnon/entities/cast.dart';
import 'package:bp_compagnon/entities/effect.dart';
import 'package:bp_compagnon/entities/rotation.dart';
import 'package:bp_compagnon/main.dart';

class Boss extends Character {
  Map<int, bool> characteristics;
  @override

  Boss(String name, this.characteristics) : super(name, Cast.boss, position: -1);
}

class Player extends Character {
  Player(String name, Cast cast) : super(name, cast, position: 2);
}

class Character {
  String name;
  Cast cast;
  int position;

  int hp = 30;
  int energy = 8;
  EffectRotation effects = EffectRotation();
  StateRotation states = StateRotation();
  CooldownRotation cooldowns = CooldownRotation();

  Character(this.name, this.cast, {required this.position});

  void rotate() {
    effects.rotate();
    states.rotate();
    cooldowns.rotate();
  }

  get isCapable => hp > 0 && states.actual is RegularState;

  get isProtected => effects.isImmune || effects.hasShield;

  void takeDamage(int value) {
    hp -= value;
  }

  void getEnergy(int value) {
    energy += value;
  }

  void getEffect(Effect effect, int index) {
    effects.push(effect, index);
  }
}

class Target {
  static Character self = game.turn.player;
  static List<Character> players = game.sidekick != null ? [game.leader, game.sidekick!] : [game.leader];
  static Player leader = game.leader;
  static Player sidekick = game.sidekick ?? game.leader;
  static Boss boss = game.boss;
}