abstract class Effect {
  String name;
  int value;

  Effect(this.name, this.value);

  void add(int v);
  void remove();
}