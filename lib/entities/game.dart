import 'package:bp_compagnon/entities/character.dart';
import 'package:bp_compagnon/entities/play.dart';
import 'package:bp_compagnon/entities/skill.dart';
import 'package:bp_compagnon/entities/turn.dart';

class Game {
  int players;
  Player leader;
  Player? sidekick;
  Boss boss;
  late Turn turn;
  List<Turn> history = [];
  List<Play> playingTurn = [];

  Game({required this.boss, required this.leader, this.sidekick, this.players = 1});

  void playSkill(Player player, Skill skill) {
    playingTurn.add(Play(player: player, skill: skill));

    if (playingTurn.length == players + 1) {
      takeTurn();
    }
  }

  void takeTurn() {
    turn = Turn(playingTurn);

    turn.resolve();

    history.add(turn);

    playingTurn = [];
  }
}