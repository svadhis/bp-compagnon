import 'package:bp_compagnon/entities/character.dart';
import 'package:bp_compagnon/entities/skill.dart';

class Play {
  Player player;
  Skill skill;

  Play({required this.player, required this.skill});
}