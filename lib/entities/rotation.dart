import 'package:bp_compagnon/data/effects.dart';
import 'package:bp_compagnon/data/player_states.dart';
import 'package:bp_compagnon/entities/effect.dart';
import 'package:bp_compagnon/entities/player_state.dart';
import 'package:bp_compagnon/entities/skill.dart';

class CooldownRotation extends Rotation<List<Skill>?> {
  @override
  List<List<Skill>?> items = List.filled(9, null);
}

class StateRotation extends Rotation<PlayerState?> {
  @override
  List<PlayerState?> items = List.filled(9, null);

  PlayerState get actual => items.reduce((value, element) => element ?? RegularState())!;
}

class EffectRotation extends Rotation<Effect?> {
  @override
  List<Effect?> items = List.filled(9, null);

  bool get isImmune => items.whereType<ImmuneEffect>().isNotEmpty;

  bool get hasShield => items.whereType<ShieldEffect>().isNotEmpty;

  void push(Effect effect, int index) {
    bool inserted = false;
    for (var i = index; i >= 0 && !inserted; i--) {
      if (items[i] is! Effect) {
        items[i] = effect;
        inserted = true;
      }
    }
  }
}

abstract class Rotation<i> {
  abstract List<i?> items;

  get out => items.first;

  void rotate() {
    for (var i = 1; i < items.length; i++) {
      items[i - 1] = items[i];
    }

    items.last = null;
  }
}