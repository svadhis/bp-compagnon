import 'package:bp_compagnon/entities/action.dart';
import 'package:bp_compagnon/entities/cast.dart';
import 'package:bp_compagnon/entities/character.dart';

class Skill {
  String name;
  Cast cast;
  bool advantage = false;
  int energy = 0;
  List<Action> actions;

  Skill(this.name, this.cast, {required this.actions});

  bool isCastable(Player player) => energy <= player.energy && player.isCapable;
}