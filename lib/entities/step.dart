import 'package:bp_compagnon/entities/character.dart';
import 'package:bp_compagnon/main.dart';

class Step {
  int value;
  int rotations;
  Function application;

  Step({required this.application, this.value = 1, this.rotations = 0});

  void resolve() {}
}

// class Target {
//   static List<Character> self = [game.turn.player];
//   static List<Character> players = game.sidekick != null ? [game.leader, game.sidekick!] : [game.leader];
//   static List<Player> leader = [game.leader];
//   static List<Player> sidekick = [game.sidekick ?? game.leader];
//   static List<Boss> boss = [game.boss];
// }