import 'package:bp_compagnon/entities/character.dart';
import 'package:bp_compagnon/entities/play.dart';

class Turn {
  List<Play> plays;
  late Character player;

  Turn(this.plays);

  List<Play> playsInOrder() {
    List<Play> playsInOrder = [];

    for (var i = plays.length - 1; i >= 0; i--) {
      if (plays[i].skill.advantage) {
        playsInOrder.insert(0, plays[i]);
        plays.removeAt(i);
      }
    }

    playsInOrder.addAll(plays);

    return playsInOrder;
  }

  void resolve() {
    List<Play> plays = playsInOrder();

    // Action 1
    for (var play in plays) {
      player = play.player;
      if (player.isCapable()) play.skill.actions.first.resolve();
    }

    // Action 2
    for (var play in plays) {
      player = play.player;
      if (player.isCapable()) play.skill.actions[1].resolve();
    }

    // Action 3
    for (var play in plays) {
      player = play.player;
      if (player.isCapable()) play.skill.actions.last.resolve();
    }
  }
}