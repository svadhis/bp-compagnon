// ignore_for_file: prefer_const_constructors

import 'package:bp_compagnon/data/boss.dart';
import 'package:bp_compagnon/entities/cast.dart';
import 'package:bp_compagnon/entities/character.dart';
import 'package:flutter/material.dart';

import 'entities/game.dart';

Boss boss = Essen();
Player leader = Player('Svadhis', Cast.swordsman);
Player sidekick = Player('Noscea', Cast.elementalist);

Game game = Game(boss: boss, leader: leader, sidekick: sidekick, players: 2);

void main() {

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BP Compagnon',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.red,
      ),
      home: const MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {


  late Game game;

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('BP Compagnon'),),
      body: Container(
        padding: EdgeInsets.all(12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Center(
                child: Column(
                  children: [
                    Text(boss.name),
                    CharacterInfos(boss),
                    CharacterRotation(boss)
                  ],
                )
              )
            ),
            Container(
              child: Center(
                child: Column(
                  children: [
                    Text(leader.name),
                    CharacterInfos(leader),
                    CharacterRotation(leader)
                  ],
                )
              )
            ),
            Container(
              child: Center(
                child: Column(
                  children: [
                    Text(sidekick.name),
                    CharacterInfos(sidekick),
                    CharacterRotation(sidekick)
                  ],
                )
              )
            ),
          ]
        )
      ),
    );
  }
}

class CharacterInfos extends StatelessWidget {
  const CharacterInfos(this.character, {Key? key}) : super(key: key);

  final Character character;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('HP: ' + character.hp.toString()),
          Text('Energy: ' + character.energy.toString()),
          Text('Position: ' + character.position.toString()),
        ],
      ),
    );
  }
}

class CharacterRotation extends StatelessWidget {
  const CharacterRotation(this.character, {Key? key}) : super(key: key);

  final Character character;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: character.effects.items.map((e) => Text(e?.name ?? ' ', style: TextStyle(color: Colors.blue),)).toList(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: character.states.items.map((e) => Text(e?.name ?? ' ', style: TextStyle(color: Colors.blue),)).toList(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: character.cooldowns.items.map((e) => e != null ? e.isNotEmpty ? Column(
              children: e.map((se) => Text(e.first.name, style: TextStyle(color: Colors.blue),)).toList()
            ) : Text(' ') : Text(' ')).toList(),
          ),
        ],
      )
    );
  }
}